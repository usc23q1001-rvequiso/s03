year_num = input("Please input a year: \n")

if year_num.isdigit():
	year_num = int(year_num)
	if year_num <= 0:
		print("Zero or negative number is not valid. ")
		quit()
else:
	print("Please input a number. ")
	quit()

if year_num % 4 == 0:
	print(f"{year_num} is a leap year. ")
else:
	print(f"{year_num} is not a leap year. ")


row = int(input("Enter number of rows: \n"))
col = int(input("Enter number of columns: \n"))

row_ctr = 0


while row_ctr < row:
	col_ctr = 0
	while col_ctr < col:
		print("*", end = '')
		col_ctr += 1
	print("")
	row_ctr += 1
